
/**
 * WsM6LiberarRecursosServiceMessageReceiverInOut.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis2 version: 1.4.1  Built on : Aug 13, 2008 (05:03:35 LKT)
 */
        package co.net.une.www.svc;

        /**
        *  WsM6LiberarRecursosServiceMessageReceiverInOut message receiver
        */

        public class WsM6LiberarRecursosServiceMessageReceiverInOut extends org.apache.axis2.receivers.AbstractInOutMessageReceiver{


        public void invokeBusinessLogic(org.apache.axis2.context.MessageContext msgContext, org.apache.axis2.context.MessageContext newMsgContext)
        throws org.apache.axis2.AxisFault{

        try {

        // get the implementation class for the Web Service
        Object obj = getTheImplementationObject(msgContext);

        WsM6LiberarRecursosServiceSkeleton skel = (WsM6LiberarRecursosServiceSkeleton)obj;
        //Out Envelop
        org.apache.axiom.soap.SOAPEnvelope envelope = null;
        //Find the axisOperation that has been set by the Dispatch phase.
        org.apache.axis2.description.AxisOperation op = msgContext.getOperationContext().getAxisOperation();
        if (op == null) {
        throw new org.apache.axis2.AxisFault("Operation is not located, if this is doclit style the SOAP-ACTION should specified via the SOAP Action to use the RawXMLProvider");
        }

        java.lang.String methodName;
        if((op.getName() != null) && ((methodName = org.apache.axis2.util.JavaUtils.xmlNameToJava(op.getName().getLocalPart())) != null)){

        

            if("M6LiberarRecursosOperation".equals(methodName)){
                
                co.net.une.www.ncainvm6.M6LiberarRecursos m6LiberarRecursos9 = null;
	                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedParam =
                                                             (co.net.une.www.ncainvm6.M6LiberarRecursos)fromOM(
                                    msgContext.getEnvelope().getBody().getFirstElement(),
                                    co.net.une.www.ncainvm6.M6LiberarRecursos.class,
                                    getEnvelopeNamespaces(msgContext.getEnvelope()));
                                                
                                               m6LiberarRecursos9 =
                                                   
                                                   
                                                           wrapM6LiberarRecursosOperation(
                                                       
                                                        

                                                        
                                                       skel.M6LiberarRecursosOperation(
                                                            
                                                                getFechaSolicitud(wrappedParam)
                                                            ,
                                                                getIdCuentaDomiciliaria(wrappedParam)
                                                            ,
                                                                getOfertaEconomica(wrappedParam)
                                                            ,
                                                                getDetalleRespuesta(wrappedParam)
                                                            ,
                                                                getAccesos(wrappedParam)
                                                            ,
                                                                getNotas(wrappedParam)
                                                            ,
                                                                getAtributos(wrappedParam)
                                                            )
                                                    
                                                         )
                                                     ;
                                            
                                        envelope = toEnvelope(getSOAPFactory(msgContext), m6LiberarRecursos9, false);
                                    
            } else {
              throw new java.lang.RuntimeException("method not found");
            }
        

        newMsgContext.setEnvelope(envelope);
        }
        }
        catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
        }
        
        //
            private  org.apache.axiom.om.OMElement  toOM(co.net.une.www.ncainvm6.M6LiberarRecursos param, boolean optimizeContent)
            throws org.apache.axis2.AxisFault {

            
                        try{
                             return param.getOMElement(co.net.une.www.ncainvm6.M6LiberarRecursos.MY_QNAME,
                                          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
                        } catch(org.apache.axis2.databinding.ADBException e){
                            throw org.apache.axis2.AxisFault.makeFault(e);
                        }
                    

            }
        
                    private  org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, co.net.une.www.ncainvm6.M6LiberarRecursos param, boolean optimizeContent)
                        throws org.apache.axis2.AxisFault{
                      try{
                          org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
                           
                                    emptyEnvelope.getBody().addChild(param.getOMElement(co.net.une.www.ncainvm6.M6LiberarRecursos.MY_QNAME,factory));
                                

                         return emptyEnvelope;
                    } catch(org.apache.axis2.databinding.ADBException e){
                        throw org.apache.axis2.AxisFault.makeFault(e);
                    }
                    }
                    

                        private co.net.une.www.ncainvm6.UTCDate getFechaSolicitud(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                        
                                return wrappedType.getM6LiberarRecursos().getFechaSolicitud();
                            
                        }
                     

                        private java.lang.String getIdCuentaDomiciliaria(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                        
                                return wrappedType.getM6LiberarRecursos().getIdCuentaDomiciliaria();
                            
                        }
                     

                        private java.lang.String getOfertaEconomica(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                        
                                return wrappedType.getM6LiberarRecursos().getOfertaEconomica();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Detallerespuestatype getDetalleRespuesta(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                        
                                return wrappedType.getM6LiberarRecursos().getDetalleRespuesta();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listaaccesosaliberartype getAccesos(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                        
                                return wrappedType.getM6LiberarRecursos().getAccesos();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listanotastype getNotas(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                        
                                return wrappedType.getM6LiberarRecursos().getNotas();
                            
                        }
                     

                        private co.net.une.www.ncainvm6.Listaatributostype getAtributos(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                        
                                return wrappedType.getM6LiberarRecursos().getAtributos();
                            
                        }
                     
                        private co.net.une.www.ncainvm6.M6LiberarRecursosType getM6LiberarRecursosOperation(
                        co.net.une.www.ncainvm6.M6LiberarRecursos wrappedType){
                            return wrappedType.getM6LiberarRecursos();
                        }
                        
                        
                    
                         private co.net.une.www.ncainvm6.M6LiberarRecursos wrapM6LiberarRecursosOperation(
                            co.net.une.www.ncainvm6.M6LiberarRecursosType innerType){
                                co.net.une.www.ncainvm6.M6LiberarRecursos wrappedElement = new co.net.une.www.ncainvm6.M6LiberarRecursos();
                                wrappedElement.setM6LiberarRecursos(innerType);
                                return wrappedElement;
                         }
                    


        /**
        *  get the default envelope
        */
        private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory){
        return factory.getDefaultEnvelope();
        }


        private  java.lang.Object fromOM(
        org.apache.axiom.om.OMElement param,
        java.lang.Class type,
        java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault{

        try {
        
                if (co.net.une.www.ncainvm6.M6LiberarRecursos.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.M6LiberarRecursos.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
                if (co.net.une.www.ncainvm6.M6LiberarRecursos.class.equals(type)){
                
                           return co.net.une.www.ncainvm6.M6LiberarRecursos.Factory.parse(param.getXMLStreamReaderWithoutCaching());
                    

                }
           
        } catch (java.lang.Exception e) {
        throw org.apache.axis2.AxisFault.makeFault(e);
        }
           return null;
        }



    

        /**
        *  A utility method that copies the namepaces from the SOAPEnvelope
        */
        private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env){
        java.util.Map returnMap = new java.util.HashMap();
        java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
        while (namespaceIterator.hasNext()) {
        org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
        returnMap.put(ns.getPrefix(),ns.getNamespaceURI());
        }
        return returnMap;
        }

        private org.apache.axis2.AxisFault createAxisFault(java.lang.Exception e) {
        org.apache.axis2.AxisFault f;
        Throwable cause = e.getCause();
        if (cause != null) {
            f = new org.apache.axis2.AxisFault(e.getMessage(), cause);
        } else {
            f = new org.apache.axis2.AxisFault(e.getMessage());
        }

        return f;
    }

        }//end of class
    